-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 16-02-2021 a las 17:29:35
-- Versión del servidor: 10.3.25-MariaDB-0ubuntu1
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `oic-be-test-1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `product_id`, `date`) VALUES
(1, 1, 3, '2021-02-16 21:51:14'),
(2, 1, 2, '2021-02-16 21:51:14'),
(3, 1, 5, '2021-02-16 21:51:14'),
(4, 2, 5, '2021-02-16 21:53:57'),
(5, 2, 1, '2021-02-16 21:53:57'),
(6, 2, 2, '2021-02-16 21:56:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `img`, `subscription_id`, `date_created`) VALUES
(1, 'Item 1', '25.99', 'file1.jpg', 0, '2021-02-16 22:07:35'),
(2, 'Item 2', '25.99', 'file2.jpg', 0, '2021-02-16 22:07:39'),
(3, 'Item 3', '35.99', 'file3.jpg', 0, '2021-02-16 22:07:43'),
(4, 'Item 4', '15.99', 'file4.jpg', 1, '2021-02-16 21:48:15'),
(5, 'Item 5', '75.99', 'file5.jpg', 2, '2021-02-16 21:48:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subscription`
--

CREATE TABLE `subscription` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `term` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `subscription`
--

INSERT INTO `subscription` (`id`, `name`, `term`, `date_created`) VALUES
(1, 'Subscription 1', '1 month', '2021-02-16 21:46:27'),
(2, 'Subscription 2', '1 year', '2021-02-16 21:47:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `logged_in` int(1) NOT NULL,
  `date` datetime NOT NULL,
  `token_login` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `name`, `email`, `logged_in`, `date`, `token_login`, `date_created`) VALUES
(1, 'johndoe', '81dc9bdb52d04dc20036dbd8313ed055', 'John Doe', 'jdoe@somesite.com', 1, '2021-02-16 16:50:02', '23f3a091fc07a2e9a4067c14acf46968a4e176f0', '2021-02-16 21:50:02'),
(2, 'janesmith', '912e79cd13c64069d91da65d62fbb78c', 'Jane Smith', 'jsmith@somesite.com', 1, '2021-02-16 16:53:18', '819f583ddf6a22606a26ac1cd38aa122bdf3d9fd', '2021-02-16 21:53:18'),
(3, 'chrisjohnson', '81b073de9370ea873f548e31b8adc081', 'Chris Johnson', 'cjohnson@somesite.com', 1, '2021-02-16 16:54:54', '560b9146d4a31d09955af2f4e787cef5f1b6d950', '2021-02-16 21:54:54'),
(4, 'markjacobs', 'def7924e3199be5e18060bb3e1d547a7', 'Mark Jacobs', 'mjacobs@somesite.com', 1, '2021-02-16 16:55:29', '6f8cb322089115749762e6f3464041947e3d3a96', '2021-02-16 21:55:29'),
(5, 'cindyheathers', '6562c5c1f33db6e05a082a88cddab5ea', 'Cindy Heathers', 'cindyheathers@somesite.com', 1, '2021-02-16 16:56:00', 'd664448cbb8f20d4984f3fea787c8d6bf7528e93', '2021-02-16 21:56:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
