<?php
class EntidadBase{
    private $table;
    private $db;
    private $conectar;

    public function __construct($table) {
        $this->table=(string) $table;
        
		require_once 'database.php';
        $this->conectar=new DataBase();
        $this->db=$this->conectar->open_connection();
        #exit;
		//$this->conectar = null;
    }
    
    public function getConectar(){
        return $this->conectar;
    }
    
    public function db(){
        return $this->db;
    }
}
