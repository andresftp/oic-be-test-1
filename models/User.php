<?php

include_once 'conf/EntidadBase.php';
class User extends EntidadBase
{
    public $id;
    public $username;
    public $password;
    public $name;
    public $email;
    public $logged_in;
    public $token_login;

    public $table = "user";

    public function __construct()
    {
        parent::__construct($this->table);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getLoggedIn()
    {
        return $this->logged_in;
    }

    /**
     * @param mixed $logged_in
     */
    public function setLoggedIn($logged_in)
    {
        $this->logged_in = $logged_in;
    }

    /**
     * @return mixed
     */
    public function getTokenLogin()
    {
        return $this->token_login;
    }

    /**
     * @param mixed $token_login
     */
    public function setTokenLogin($token_login)
    {
        $this->token_login = $token_login;
    }

    //Insert update usuer
    public function insertUpdateUser($act){
        if($act=='u'){
            $start = "UPDATE ";
            $end = " WHERE id='".$this->id."'";
        }elseif($act=='i'){
            $start = "INSERT INTO ";
            $end = "";
        }

        $this->query = $start." user SET
            username='".$this->username."',
            password='".$this->password."',
            name='".$this->name."',
            email='".$this->email."'
        ".$end;

        $con = $this->db();
        $save = $con->query($this->query);
        $lstid = $con->insert_id;
        if (!$save) {
            throw new Exception(mysqli_error($con) . "[ $this->query]");
        }
        return array($save, $lstid);

    }

    public function setTokenLoginUser(){

        $this->query = " UPDATE user SET
            logged_in='".$this->logged_in."',
            date=NOW(),
            token_login='".$this->token_login."'
            WHERE id = '".$this->id."'
        ";

        $con = $this->db();
        $save = $con->query($this->query);
        if (!$save) {
            throw new Exception(mysqli_error($con) . "[ $this->query]");
        }
        return $save;
    }


    public function getUser($cond=''){
        $query = "SELECT * FROM user 
                    WHERE id >0 ".$cond;
        $con = $this->db();
        $res = $con->query($query);
        while ($row = $res->fetch_assoc()) {
            $resultSet[] = $row;
        }
        return $resultSet;
    }


}