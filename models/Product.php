<?php

include_once 'conf/EntidadBase.php';
class Product extends EntidadBase
{
    public $id;
    public $name;
    public $price;
    public $img;
    public $subscription_id;

    public $table = "product";

    public function __construct()
    {
        parent::__construct($this->table);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return mixed
     */
    public function getSubscriptionId()
    {
        return $this->subscription_id;
    }

    /**
     * @param mixed $subscription_id
     */
    public function setSubscriptionId($subscription_id)
    {
        $this->subscription_id = $subscription_id;
    }

    //Insert update product
    public function insertUpdateProd($act){
        if($act=='u'){
            $start = "UPDATE ";
            $end = " WHERE id='".$this->id."'";
        }elseif($act=='i'){
            $start = "INSERT INTO ";
            $end = "";
        }

        $this->query = $start." product SET
            name='".$this->name."',
            price='".$this->price."',
            img='".$this->img."',
            subscription_id=".$this->subscription_id."
        ".$end;

        $con = $this->db();
        $save = $con->query($this->query);
        $lstid = $con->insert_id;
        if (!$save) {
            throw new Exception(mysqli_error($con) . "[ $this->query]");
        }
        return array($save, $lstid);

    }

    public function getProduct($cond=''){
        $query = "SELECT * FROM product 
                    WHERE id >0 ".$cond;
        $con = $this->db();
        $res = $con->query($query);
        while ($row = $res->fetch_assoc()) {
            $resultSet[] = $row;
        }
        return $resultSet;
    }
}