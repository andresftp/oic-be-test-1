<?php

include_once 'conf/EntidadBase.php';
class Subscription extends EntidadBase
{
    public $id;
    public $name;
    public $term;

    public $table = "subscription";

    public function __construct()
    {
        parent::__construct($this->table);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param mixed $term
     */
    public function setTerm($term)
    {
        $this->term = $term;
    }



    //Insert update subscription
    public function insertUpdateSubscription($act){
        if($act=='u'){
            $start = "UPDATE ";
            $end = " WHERE id='".$this->id."'";
        }elseif($act=='i'){
            $start = "INSERT INTO ";
            $end = "";
        }

        $this->query = $start." subscription SET
            name='".$this->name."',
            term='".$this->term."'
        ".$end;

        $con = $this->db();
        $save = $con->query($this->query);
        $lstid = $con->insert_id;
        if (!$save) {
            throw new Exception(mysqli_error($con) . "[ $this->query]");
        }
        return array($save, $lstid);

    }

    public function getSubscription($cond=''){
        $query = "SELECT * FROM subscription 
                    WHERE id >0 ".$cond;
        $con = $this->db();
        $res = $con->query($query);
        while ($row = $res->fetch_assoc()) {
            $resultSet[] = $row;
        }
        return $resultSet;
    }
}