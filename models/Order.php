<?php

include_once 'conf/EntidadBase.php';
class Order extends EntidadBase
{
    public $id;
    public $user_id;
    public $product_id;

    public $table = "orders";

    public function __construct()
    {
        parent::__construct($this->table);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    //Insert update order
    public function insertUpdateProd($act){
        if($act=='u'){
            $start = "UPDATE ";
            $end = " WHERE id='".$this->id."'";
        }elseif($act=='i'){
            $start = "INSERT INTO ";
            $end = "";
        }

        $this->query = $start." orders SET
            user_id='".$this->user_id."',
            product_id='".$this->product_id."'
        ".$end;

        $con = $this->db();
        $save = $con->query($this->query);
        $lstid = $con->insert_id;
        if (!$save) {
            throw new Exception(mysqli_error($con) . "[ $this->query]");
        }
        return array($save, $lstid);

    }

    public function getOrder($cond=''){
        $query = "SELECT * FROM orders 
                    WHERE id >0 ".$cond;
        $con = $this->db();
        $res = $con->query($query);
        while ($row = $res->fetch_assoc()) {
            $resultSet[] = $row;
        }
        return $resultSet;
    }

    public function getOrderBySubs($sort, $order){
        $query = "SELECT * FROM orders AS o
                    INNER JOIN product AS p ON p.id = o.product_id
                    WHERE p.subscription_id>0 ORDER BY ".$sort." ".$order;
        $con = $this->db();
        $res = $con->query($query);
        while ($row = $res->fetch_assoc()) {
            $resultSet[] = $row;
        }
        return $resultSet;
    }
}