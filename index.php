<?php
//Include all models os database
include_once 'models/User.php';
include_once 'models/Product.php';
include_once 'models/Subscription.php';
include_once 'models/Order.php';

$objUser = new User();
$objProd = new Product();
$objSubs = new Subscription();
$objOrder = new Order();

// the data in $_GET is trimmed
array_filter($_GET, 'trim_value');

//Router of task
switch ($_GET['task']){
    case 'register':  //Task register user

        //Filter and snitize data
        $email = filter_var($_GET['email'],FILTER_SANITIZE_EMAIL);
        $username = filter_var($_GET['username'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $password = filter_var($_GET['password'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $name = filter_var($_GET['name'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        //Check if username or email exist
        $resUser = $objUser->getUser(" AND ( email='".$email."' OR username='".$username."')");
        if(count($resUser)>0){
            $result = array(
                'status'=>'error',
                'code'=>200,
                'msj'=>'User or email already registered',
                'data'=>null
            );
        }else {
            //Registes user
            $insUser = new User();
            $insUser->setEmail($email);
            $insUser->setUsername($username);
            $insUser->setPassword(md5($password));
            $insUser->setName($name);

            //Insert data in database
            try {
                $insUser->insertUpdateUser('i');
                $result = array(
                    'status' => 'ok',
                    'code' => 200,
                    'msj' => 'Registered user successfully',
                    'data' => null
                );
            } catch ( Exception $e){
                $result = array(
                    'status' => 'error',
                    'code' => 200,
                    'msj' => $e->getMessage(),
                    'data' => null
                );
            }

        }
        break;
    case 'login':  //Login User

        //Filter and snitize data
        $username = filter_var($_GET['username'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $password = filter_var($_GET['password'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        //vERIFY USER AND PASSWORD
        $resUser = $objUser->getUser(" AND username='".$username."' AND password='".md5($password)."'");
        if(count($resUser)>0){
            //Create token logged
            $token = sha1($resUser[0]['id'].date('Y-m-d H:i:s'));

            $udpUser = new User();
            $udpUser->setId($resUser[0]['id']);
            $udpUser->setLoggedIn(1);
            $udpUser->setTokenLogin($token);

            //update data login
            try {
                $udpUser->setTokenLoginUser();
            } catch ( Exception $e){
                $result = array(
                    'status' => 'error',
                    'code' => 200,
                    'msj' => $e->getMessage(),
                    'data' => null
                );
            }

            //Retturn data user logged
            $dataUser = $objUser->getUser(" AND id='".$resUser[0]['id']."'");
            $result = array(
                'status'=>'ok',
                'code'=>200,
                'msj'=>'User logged in successfully',
                'data'=>$dataUser[0]
            );
        }else {
            $result = array(
                'status' => 'error',
                'code' => 200,
                'msj' => 'invalid username or password',
                'data' => null
            );
        }
        break;
    case 'addproduct':

        //Filter and snitize data
        $name = filter_var($_GET['name'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $price = filter_var($_GET['price'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $img = filter_var($_GET['img'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $subscription_id = filter_var($_GET['subscription_id'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $subscription_id = ($subscription_id=='')?0:$subscription_id;

        //Register product
        $insProd = new Product();
        $insProd->setName($name);
        $insProd->setPrice($price);
        $insProd->setImg($img);
        $insProd->setSubscriptionId($subscription_id);

        //Insert data in database
        try {
            $insProd->insertUpdateProd('i');
            $result = array(
                'status' => 'ok',
                'code' => 200,
                'msj' => 'Registered product successfully',
                'data' => null
            );
        } catch ( Exception $e){
            $result = array(
                'status' => 'error',
                'code' => 200,
                'msj' => $e->getMessage(),
                'data' => null
            );
        }
        break;
    case 'addsubscription':

        //Filter and snitize data
        $name = filter_var($_GET['name'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $term = filter_var($_GET['term'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        //Register product
        $insSubs = new Subscription();
        $insSubs->setName($name);
        $insSubs->setTerm($term);

        //Insert data in database
        try {
            $insSubs->insertUpdateSubscription('i');
            $result = array(
                'status' => 'ok',
                'code' => 200,
                'msj' => 'Registered subscription successfully',
                'data' => null
            );
        } catch ( Exception $e){
            $result = array(
                'status' => 'error',
                'code' => 200,
                'msj' => $e->getMessage(),
                'data' => null
            );
        }
        break;

    case 'placeorder':

        $user = filter_var($_GET['user'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $product_id = filter_var($_GET['product_id'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        //Get user by username
        $dataUser = $objUser->getUser(" AND username='".$user."'");
        if(count($dataUser)>0){

            //Extract products
            $products = explode(',',$product_id);
            foreach ($products AS $product) {
                //Register product
                $insOrd = new Order();
                $insOrd->setProductId($product);
                $insOrd->setUserId($dataUser[0]['id']);
                //Insert data in database
                try {
                    $insOrd->insertUpdateProd('i');
                    $result = array(
                        'status' => 'ok',
                        'code' => 200,
                        'msj' => 'Registered order successfully',
                        'data' => null
                    );
                } catch (Exception $e) {
                    $result = array(
                        'status' => 'error',
                        'code' => 200,
                        'msj' => $e->getMessage(),
                        'data' => null
                    );
                }
            }
        }else{
            $result = array(
                'status' => 'error',
                'code' => 200,
                'msj' => 'User no registered',
                'data' => null
            );
        }
        break;
    case 'search':
        $show_users_logged_in = filter_var($_GET['show_users_logged_in'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $sort = filter_var($_GET['sort'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $order = filter_var($_GET['order'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $order_type = filter_var($_GET['order_type'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        if($show_users_logged_in==1) {
            //Get user by username
            $dataUser = $objUser->getUser(" AND logged_in='" . $show_users_logged_in . "' ORDER BY " . $sort . " " . $order);
            if (count($dataUser) > 0) {
                $result = array(
                    'status' => 'ok',
                    'code' => 200,
                    'msj' => 'Users found',
                    'data' => $dataUser
                );

            } else {
                $result = array(
                    'status' => 'ok',
                    'code' => 200,
                    'msj' => 'No information was found with the search parameters',
                    'data' => null
                );
            }
        }elseif($order_type!=''){
            $dataOrder = $objOrder->getOrderBySubs($sort,$order);
            if (count($dataOrder) > 0) {
                $result = array(
                    'status' => 'ok',
                    'code' => 200,
                    'msj' => 'Orders found',
                    'data' => $dataOrder
                );

            } else {
                $result = array(
                    'status' => 'ok',
                    'code' => 200,
                    'msj' => 'No information was found with the search parameters',
                    'data' => null
                );
            }
        }
        break;
    default:
        $result = array(
            'status'=>'ok',
            'code'=>200,
            'msj'=>'No task selected',
            'data'=>null
        );
        break;
}

http_response_code($result['code']);
echo json_encode($result);